//1.1 Crea un bucle for que vaya desde 0 a 9 y muestralo por consola.
let num = 0;
let numArray = [];
while (num <= 9) {
    numArray[num] = num;
    num++;
}
console.log('1.1 ->', numArray);

/*1.2 Crea un bucle for que vaya desde 0 a 9 y muestralo por consola solo 
cuando el resto del numero dividido entre 2 sea 0. */
let numArray2 = [];

for (let i = 0; i <= 9; i++) {
    if ((i % 2) === 0) {
        numArray2.push(i);
    }

}
console.log('1.2 ->', numArray2);

/* 1.3 Crea un bucle para conseguir dormir contando ovejas. 
Este bucle empieza en 0 y termina en 10. 
Muestra por consola un mensaje diciendo 'Intentando dormir' en cada vuelta del bucle 
y cambia el mensaje en la última vuelta a 'Dormido!'. */
console.log('1.3 ->');
let count = 0;

while (count <= 10) {
    if (count === 10) {
        console.log('Dormido!');
    } else {
        console.log(count,'Intentando dormir');
    }
    count++;
}